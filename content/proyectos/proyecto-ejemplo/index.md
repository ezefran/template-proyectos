---
title: "Proyecto de Ejemplo"
extracto: "Descripcion del proyecto"
featuredImage: "images/placeholder_1.png"
codigo: "/"
demo: "/"
documentacion: "/"
prioridad: 1
---

Descripción detallada del proyecto

# Características

- Característica 1
- Característica 2
- Característica 3

# Tecnologias

- Tecnología 1
- Tecnología 2
- Tecnología 3
