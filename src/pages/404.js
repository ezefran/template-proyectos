import React from "react"
import styles from "./404.module.scss"

export default function NotFound() {
  return (
    <main className={styles.notFound}>
      <h1>404</h1>
      <h2>Página no encontrada</h2>
    </main>
  )
}
