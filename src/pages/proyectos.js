import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import styles from "./proyectos.module.scss"
import Proyecto from "../components/proyecto"

export default function Home({ data }) {
  data.allMarkdownRemark.edges.sort(
    (a, b) => a.node.frontmatter.prioridad - b.node.frontmatter.prioridad
  )
  return (
    <Layout>
      <main className={styles.proyectos}>
        {data.allMarkdownRemark.edges.map(({ node }) => {
          const imageEdge = data.allFile.edges.find(imageNode => {
            return (
              imageNode.node.absolutePath.includes(
                `proyectos${node.fields.slug}`
              ) &&
              node.frontmatter.featuredImage.name.includes(imageNode.node.name)
            )
          })

          const imagen = imageEdge ? imageEdge.node.childImageSharp.fluid : null

          return (
            <Proyecto
              key={node.id}
              titulo={node.frontmatter.title}
              imagen={imagen}
              extracto={node.frontmatter.extracto}
              slug={node.fields.slug}
            />
          )
        })}
      </main>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/proyectos/" } }) {
      edges {
        node {
          id
          frontmatter {
            featuredImage {
              name
            }
            title
            extracto
            prioridad
          }
          fields {
            slug
          }
        }
      }
    }
    allFile(
      filter: {
        absolutePath: { regex: "/proyectos/" }
        extension: { ne: "md" }
      }
    ) {
      edges {
        node {
          name
          absolutePath
          childImageSharp {
            fluid {
              originalName
              src
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
