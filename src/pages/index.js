import { Link } from "gatsby"
import React from "react"
import Layout from "../components/layout"
import LinkExterno from "../components/link-externo"
import styles from "./index.module.scss"
import { FaGithub, FaGitlab } from "react-icons/fa"

export default function Home() {
  return (
    <Layout>
      <main className={styles.index}>
        <section className={styles.contenido}>
          <h1>Nombre Apellido</h1>
          <h2>Título</h2>
        </section>
        <section className={styles.repos}>
          <span className={styles.colorPrimario}>
            <LinkExterno link="/">
              <FaGitlab />
            </LinkExterno>
          </span>
          <span className={styles.colorSecundario}>
            <LinkExterno link="/">
              <FaGithub />
            </LinkExterno>
          </span>
        </section>
        <section className={styles.cta}>
          <Link to="/proyectos">Ver Proyectos</Link>
        </section>
      </main>
    </Layout>
  )
}
