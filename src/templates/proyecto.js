import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import styles from "./proyecto.module.scss"
import { SwiperSlide } from "swiper/react"
import Lightbox from "react-awesome-lightbox"
import "react-awesome-lightbox/build/style.css"
import ResponsiveSwiper from "../components/responsive-swiper"
import { FaBook, FaCode, FaLaptop } from "react-icons/fa"
import LinkExterno from "../components/link-externo"
import Img from "gatsby-image"

export default function Proyecto({ data }) {
  const [imagenSeleccionada, setImagenSeleccionada] = useState(null)

  const proyecto = data.markdownRemark
  const imagenes = data.allFile.edges.map(
    edge => edge.node.childImageSharp.fluid
  )
  imagenes.sort((a, b) => a.originalName.localeCompare(b.originalName))
  const unaImagen = imagenes.length === 1
  const sinImagenes = imagenes.length <= 0
  const slides = imagenes.map(imagen => (
    <SwiperSlide key={imagen.originalName}>
      <div
        className={styles.imgWrapper}
        onClick={() => setImagenSeleccionada(imagen.originalName)}
        onKeyDown={() => setImagenSeleccionada(imagen.originalName)}
        role="presentation"
      >
        <Img
          fluid={imagen}
          style={{ height: "auto", width: "100%" }}
          imgStyle={{ minHeight: "150px", width: "100%", objectFit: "contain" }}
          alt={imagen.name}
        />
      </div>
    </SwiperSlide>
  ))

  const atribs = proyecto.frontmatter

  return (
    <Layout>
      <main className={styles.proyecto}>
        <article>
          <ResponsiveSwiper
            slides={slides}
            mobileStyle={styles.swiperMobile}
            desktopStyle={styles.swiperDesktop}
            paginationStyle={styles.swiperPagination}
          />
          <div className={styles.titulo}>
            <div className={styles.botonesTitulo}>
              <LinkExterno link={atribs.codigo}>
                <FaCode /> Código
              </LinkExterno>
              <LinkExterno link={atribs.documentacion}>
                <FaBook /> Documentación
              </LinkExterno>
              <LinkExterno link={atribs.demo}>
                <FaLaptop /> Demo
              </LinkExterno>
            </div>

            <h1>{atribs.title}</h1>
          </div>
          <div
            className={styles.contenido}
            dangerouslySetInnerHTML={{ __html: proyecto.html }}
          />
        </article>
      </main>

      {imagenSeleccionada && !sinImagenes ? (
        <Lightbox
          image={unaImagen ? imagenes[0].src : null}
          images={unaImagen ? null : imagenes.map(imagen => imagen.src)}
          startIndex={imagenes.findIndex(
            imagen => imagen.originalName === imagenSeleccionada
          )}
          onClose={() => setImagenSeleccionada(null)}
          allowRotate={false}
        />
      ) : null}
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!, $regex: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        codigo
        documentacion
        demo
      }
    }
    allFile(
      filter: { absolutePath: { regex: $regex }, extension: { ne: "md" } }
    ) {
      edges {
        node {
          childImageSharp {
            fluid {
              originalName
              src
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
