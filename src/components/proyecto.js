import React from "react"
import styles from "./proyecto.module.scss"
import { Link } from "gatsby"
import Img from "gatsby-image"

export default function Proyecto({ titulo, extracto, imagen, slug }) {
  return (
    <Link to={slug}>
      <article className={styles.proyecto}>
        <div className={styles.imgWrapper}>
          <Img
            fluid={imagen}
            style={{ height: "auto", width: "100%" }}
            imgStyle={{ height: "170px", width: "100%", objectFit: "contain" }}
            className={styles.img}
            alt={imagen.name}
          />
        </div>
        <h2>{titulo}</h2>
        <p>{extracto}</p>
      </article>
    </Link>
  )
}
