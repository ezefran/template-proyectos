import React from "react"
import SwiperCore, { Pagination, Navigation } from "swiper"
import { Swiper } from "swiper/react"
import "swiper/swiper-bundle.min.css"

SwiperCore.use([Pagination, Navigation])

export default function ResponsiveSwiper(props) {
  return (
    <div>
      <Swiper
        className={props.mobileStyle}
        pagination={{ clickable: true, el: "." + props.paginationStyle }}
      >
        {props.slides}
      </Swiper>
      <div className={props.paginationStyle}></div>
      <Swiper
        className={props.desktopStyle}
        spaceBetween={5}
        slidesPerView={2}
        centeredSlides
        navigation
      >
        {props.slides}
      </Swiper>
    </div>
  )
}
