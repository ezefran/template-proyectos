import React from "react"

export default function LinkExterno(props) {
  return props.link ? (
    <a href={props.link} target="_blank" rel="noopener noreferrer">
      {props.children}
    </a>
  ) : null
}
