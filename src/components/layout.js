import React from "react"
import "normalize.css"
import styles from "./layout.module.scss"
import { Link } from "gatsby"
import { Helmet } from "react-helmet"

export default function Layout({ children }) {
  return (
    <div className={styles.Layout}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Título</title>
      </Helmet>
      <nav>
        <Link to="/">Nombre</Link>
        <ul>
          <li>
            <Link to="/" activeClassName={styles.linkActivo}>
              Inicio
            </Link>
          </li>
          <li>
            <Link to="/proyectos" activeClassName={styles.linkActivo}>
              Proyectos
            </Link>
          </li>
        </ul>
      </nav>
      {children}
      <footer><p>Nombre © 2020</p></footer>
    </div>
  )
}
