const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `content` })

    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })

    const parent = getNode(node.parent)
    const collection = parent.sourceInstanceName

    createNodeField({
      node,
      name: `collection`,
      value: collection,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
              collection
            }
            fileAbsolutePath
          }
        }
      }
    }
  `)

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    const component =
      node.fields.collection === "proyectos"
        ? path.resolve(`./src/templates/proyecto.js`)
        : null

    if (!component)
      throw new Error("No existe template para: " + node.fileAbsolutePath)

    createPage({
      path: node.fields.slug,
      component: component,
      context: {
        slug: node.fields.slug,
        regex: `/${node.fields.slug}images/`,
      },
    })
  })
}
